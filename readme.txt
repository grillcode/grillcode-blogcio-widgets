=== GlobalIT Widgets ===
Tags: plugin, theme, update, updater, github, bitbucket, gitlab, remote install
Requires at least: 3.8
Tested up to: 4.2
Stable tag: master
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

Añade nuevos widgets para el blog de telefónica Global IT

== Changelog ==

= 1.1 =
* Added widget for links box

= 1.0 =
* Added taxonomy for categories
* Added new fields to links panel list

= 0.6 =
* Initial release