<?php
/**
* Plugin Name: GlobalIT Widgets
* Plugin URI: http://www.grillcode.com/grillcode-blogcio-widgets
* Version: 1.1
* Author: Javier Otero
* Author URI: http://www.grillcode.com
* Description: Widget para mostrar links
* License: GPL2
* Bitbucket Plugin URI: https://bitbucket.org/grillcode/grillcode-blogcio-widgets
* Bitbucket Branch: master
*/

class GC_GlobalIT_Widgets{
	/**
	* Constructor. Called when plugin is initialised
	*/
	function GC_GlobalIT_Widgets() {
		add_action( 'init', array(&$this, 'gc_register_custom_post_type') );
		add_action( 'init', array(&$this, 'gc_create_link_tax') );
		
		add_filter( 'manage_edit-gclink_columns', array(&$this, 'gc_link_columns' ) );
		add_action( 'manage_gclink_posts_custom_column', array(&$this, 'gc_manage_gclink_columns'), 10, 2 );

		add_action( 'add_meta_boxes', array( $this, 'gc_add_meta_box') );
		add_action( 'save_post',      array( $this, 'gc_save_meta_box_data' ) );
	}
	
	/**
	* Registers a Custom Post Type called gclink
	*/
	function gc_register_custom_post_type() {
		register_post_type('gclink', array(
            'labels' => array(
				'name'               => _x( 'Links', 'post type general name', 'grillcode' ),
				'singular_name'      => _x( 'Link', 'post type singular name', 'grillcode' ),
				'menu_name'          => _x( 'GlobalIT links', 'admin menu', 'grillcode' ),
				'name_admin_bar'     => _x( 'GlobalIT Link', 'add new on admin bar', 'grillcode' ),
				'add_new'            => _x( 'Add New', 'Link', 'grillcode' ),
				'add_new_item'       => __( 'Add New Link', 'grillcode' ),
				'new_item'           => __( 'New Link', 'grillcode' ),
				'edit_item'          => __( 'Edit Link', 'grillcode' ),
				'view_item'          => __( 'View Link', 'grillcode' ),
				'all_items'          => __( 'All Links', 'grillcode' ),
				'search_items'       => __( 'Search Links', 'grillcode' ),
				'parent_item_colon'  => __( 'Parent Links:', 'grillcode' ),
				'not_found'          => __( 'No Links found.', 'grillcode' ),
				'not_found_in_trash' => __( 'No Links found in Trash.', 'grillcode' ),
			),
            
            // Frontend
            'has_archive' => false,
            'public' => false,
            'publicly_queryable' => false,
            
            // Admin
            'capability_type' => 'post',
            //'menu_icon' => 'dashicons-admin-site',  //this only works for wp 3.8+
            'menu_position' => 10,
            'query_var' => true,
            'show_in_menu' => true,
            'show_ui' => true,
            'supports' => array(
            	'title'
            ),
        ));	
	}

	function gc_create_link_tax() {
		register_taxonomy(
			'gclinkcategory',
			'gclink',
			array(
				'label' => __( 'Category' ),
				'rewrite' => array( 'slug' => 'gclinkcategory' ),
				'hierarchical' => true,
			)
		);
	}


	/**
	 * add_meta_box
	 */
	public function gc_add_meta_box(){	 
		add_meta_box( 'cpt_examples_meta', 'Detalles del link', array( $this, 'gc_display_meta_form' ), 'gclink', 'advanced', 'high' );
	}

	
	function gc_link_columns( $columns ) {
	
		$columns = array(
			'cb' => '<input type="checkbox" />',
			'title' => __( 'Title' ),
			'gc_link_meta_field' => __( 'Url' ),
			'gclinkcategory' => __( 'Category' ),
			'date' => __( 'Date' )
		);
	
		return $columns;
	}
	
	function gc_manage_gclink_columns( $column, $post_id ) {
		global $post;
	
		switch( $column ) {
	
			case 'gc_link_meta_field' :
	
				$url = get_post_meta( $post_id, 'gc_link_meta_field', true );
				if ( empty( $url ) )
					echo  '';
				else
					echo $url;
	
				break;
	
			/* If displaying the 'genre' column. */
			case 'gclinkcategory' :
	
				/* Get the genres for the post. */
				$terms = get_the_terms( $post_id, 'gclinkcategory' );
	
				/* If terms were found. */
				if ( !empty( $terms ) ) {
	
					$out = array();
	
					/* Loop through each term, linking to the 'edit posts' page for the specific term. */
					foreach ( $terms as $term ) {
						$out[] = sprintf( '<a href="%s">%s</a>',
							esc_url( add_query_arg( array( 'post_type' => $post->post_type, 'gclinkcategory' => $term->slug ), 'edit.php' ) ),
							esc_html( sanitize_term_field( 'name', $term->name, $term->term_id, 'gclinkcategory', 'display' ) )
						);
					}
	
					/* Join the terms, separating them with a comma. */
					echo join( ', ', $out );
				}
	
				else {
					_e( '' );
				}

				break;
	
			default :
				break;
		}
	}

	/**
	 * display_meta_form	
	 */
	function gc_display_meta_form( $post ) {
	 
		wp_nonce_field( 'gc_link_meta_box', 'gc_link_meta_box_nonce' );
	 
		$my_first_field  = get_post_meta( $post->ID, 'gc_link_meta_field', true );
	 
			echo '<div class="wrap">';
			echo '<label for="gc_link_meta_field">' . _e( 'URL', 'grillcode' ) . '</label> <br/>';
			echo '<input class="widefat" type="text" id="gc_link_meta_field" name="gc_link_meta_field" value="' . esc_attr( $my_first_field ) . '"   />';
			echo '</div>';
	 
	}

	/**
	 * save_meta_box_data
	 * función llamada en el hook save_post para validar y guardar los datos.
	 */
	 
	function gc_save_meta_box_data( $post_id ){
	 
	  // Verificar que se ha declarado el nonce.
	    if ( ! isset( $_POST['gc_link_meta_box_nonce'] ) ) {
		  return;
	    }
	 
	  // Verificar que el nonce es válido.
	    if ( ! wp_verify_nonce( $_POST['gc_link_meta_box_nonce'], 'gc_link_meta_box' ) ) {
		   return;
	    }
	 
	  // Si es un autoguardado no debe hacer nada.
	    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		  return;
	    }
	 
	  // Verificar los permisos del usuario.
	    if ( isset( $_POST['post_type'] ) && $_POST['post_type'] == 'gclink' ) {
	            if ( ! current_user_can( 'edit_page', $post_id ) ) {
			     return;
		    }
	 
	    } else {
	            if ( ! current_user_can( 'edit_post', $post_id ) ) {
			     return;
		    }
	    }
	 
	   // Guardar la información en la base de datos
	    if ( isset( $_POST['gc_link_meta_field'] ) ) {
	        $my_first_meta_field = sanitize_text_field( $_POST['gc_link_meta_field'] );
		    update_post_meta( $post_id, 'gc_link_meta_field', $my_first_meta_field );
		}
	 
	}

}

$OXcpt = new GC_GlobalIT_Widgets ;

/**
 * Links widget
 */
class GC_Widget_Links extends WP_Widget {

	function __construct() {
		$widget_ops = array('description' => __( "Añade una lista de links" ) );
		parent::__construct('gcwidgetlinks', __('GC Links'), $widget_ops);
	}

	function widget( $args, $instance ) {
		extract($args, EXTR_SKIP);

		$title = isset($instance['title']) ? $instance['title'] : 'Links';
		$cat = isset($instance['cat']) ? $instance['cat'] : '';

		//$before_widget = preg_replace('/id="[^"]*"/','id="%id"', $before_widget);


        $args = array(
            'post_type' => 'gclink',
            'post_status' => 'publish',
            'posts_per_page'=> -1,
			'tax_query' => array(
			    array(
			      'taxonomy' => 'gclinkcategory',
			      'field' => 'slug',
			      'terms' => $cat
			    )
			  ),
            //'orderby' 	=> 'meta_value',
            'order' 	=> 'ASC'
        );

        $postList = get_posts( $args );

        $out = '';

        $out .= '<ul id="link-list">';

        foreach ($postList as $key => $post) {

            $custom 	= get_post_custom($post->ID);
            $url	 	= (isset($custom["gc_link_meta_field"]))?$custom["gc_link_meta_field"][0]:'';

           	$out .= '	<li><a href="' . $url . '">' . apply_filters('the_title', $post->post_title) . '</a></li>';

        }

        $out .= '</ul><!-- / event-list -->';


        echo $before_widget;
        echo '<span class="widget-icon glyphicon glyphicon-bookmark"></span><h3 class="widget-title gc-widget-links">';
        echo apply_filters('the_title',$title) . $after_title;

        //echo '<ul>';
        echo $out;
        //echo '</ul>';
        echo $after_widget;

	}

	function update( $new_instance, $old_instance ) {
		$new_instance = (array) $new_instance;
		$instance = array( 'title' => 'links', 'cat' => '' );
		foreach ( $instance as $field => $val ) {
			if ( isset($new_instance[$field]) )
				$instance[$field] = 1;
		}

		$instance['title']      = $new_instance['title'];
		$instance['cat']      = $new_instance['cat'];

		return $instance;
	}

	function form( $instance ) {

		//Defaults
		$instance = wp_parse_args( (array) $instance, array( 'title' => 'links', 'cat' => '' ) );
		
		?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>">Título:</label>
            <input class="widefat" type="text" name="<?php echo $this->get_field_name('title') ?>" id="<?php echo $this->get_field_id('title') ?> " value="<?php echo $instance['title'] ?>" >
        </p>
		<p>
			<label for="<?php echo $this->get_field_id('cat'); ?>">Categoría:</label>
			<?php 
			$terms = get_terms( 'gclinkcategory' );
			if ( $terms ) {
				printf( '<select name="%s" id="%s" class="postform">', $this->get_field_name('cat'), $this->get_field_id('cat')  );
				foreach ( $terms as $term ) {
					$selected = ( $instance['cat'] == esc_attr( $term->slug ) )?' selected':'';
					printf( '<option value="%s" %s>%s</option>', esc_attr( $term->slug ), $selected, esc_html( $term->name ) );
				}
				print( '</select>' );
			}
			?>
		</p>

<?php
	}
}

function gc_register_widgets() {
 
    register_widget( 'GC_Widget_Links' );
	register_widget( 'GC_Widget_Box' );
 
}
add_action( 'widgets_init', 'gc_register_widgets' );


class GC_Widget_Box extends WP_Widget {

	function __construct() {
		$widget_ops = array('description' => __( "Añade una caja con un link" ), 'classname' => 'box-url' );
		parent::__construct('gcwidgetbox', __('GC Caja link'), $widget_ops);
	}

	function widget( $args, $instance ) {
		extract($args, EXTR_SKIP);

        $title = $instance['title'];
        $link = $instance['link'];
		$background = $instance['background'];
		$fontColor = $instance['fontcolor'];

        $out = '';
		
		//TO-DO: move this to an external css file
		$out .= '<style>
		aside.widget.box-url {
  			background-color: ' . $background . ';
			margin: 20px 0;
			height: 75px;
		}
		.widget.box-url a {
		  color: ' . $fontColor . ';
		  padding-left: 20px;
		  height: 45px;
		  font: normal 31px/31px TelefonicaScript,Helvetica,arial,sans-serif;
		  overflow: hidden;
		  display: inline-block;
		}</style>';
		
        $out .= '<div class="box-url">';
        $out .= '<a href="' . $link . '">';
        $out .= $title;
        $out .= '</a>';
        $out .= '</div>';

        echo $before_widget;
        echo '<ul>';
        echo $out;
        echo '</ul>';
        echo $after_widget;
	}

    function form($instance) {

        $defaults = array( 'link' => '', 'title' => '', 'background' => '#003245', 'fontcolor' => '#FFFFFF' );
        $instance = wp_parse_args( (array) $instance, $defaults ); ?>

        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>">Título:</label>
            <input class="widefat" type="text" name="<?php echo $this->get_field_name('title') ?>" id="<?php echo $this->get_field_id('title') ?> " value="<?php echo $instance['title'] ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('link'); ?>">Link:</label>
            <input class="widefat" type="text" name="<?php echo $this->get_field_name('link') ?>" id="<?php echo $this->get_field_id('link') ?> " value="<?php echo $instance['link'] ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('background'); ?>">Color fondo:</label>
            <input class="widefat" type="text" name="<?php echo $this->get_field_name('background') ?>" id="<?php echo $this->get_field_id('background') ?> " value="<?php echo $instance['background'] ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('fontcolor'); ?>">Color texto:</label>
            <input class="widefat" type="text" name="<?php echo $this->get_field_name('fontcolor') ?>" id="<?php echo $this->get_field_id('fontcolor') ?> " value="<?php echo $instance['fontcolor'] ?>">
        </p>

    <?php

    }
    function update($new_instance, $old_instance) {
        // processes widget options to be saved
        $instance = $old_instance;

        $instance['link'] = $new_instance['link'];
        $instance['title'] = $new_instance['title'];
        $instance['background'] = $new_instance['background'];
        $instance['fontcolor'] = $new_instance['fontcolor'];

        return $instance;
    }
 
}

